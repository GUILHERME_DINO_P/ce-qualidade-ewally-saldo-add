/// <reference types="cypress" />


describe("Ewally - Creditar Transferência", () => {

  it('Creditar Transferência', () => {
    cy.visit("https://beta.admin.ewally.com.br/login")
    cy.get('[data-test="login-username"]').type(Cypress.env("EWALLY_USER"), { log: false })
    cy.get('[data-test="login-password"]').type(Cypress.env("EWALLY_PASS"), { log: false })
    cy.get('[data-test="login-submit"]')
      .should("have.html", "Entrar")
      .click()

    cy.get('[data-test="credit-bank-deposit-link"] > .nav-item')
      .should("be.visible")
      .click()

    cy.get('[data-testid="document-input-field"]')
      .should("be.visible")
      .type(Cypress.env("EWALLY_CPF"), { log: false })

    cy.get('[data-testid="bank-select-field"]')
      .select(Cypress.env("EWALLY_BANCO"))

    cy.get('[data-testid="amount-input-field"]')
      .type(Cypress.env('EWALLY_VALOR'));

    cy.get('[data-testid="undefined-submit-button"]')
      .click()

    cy.get('[data-testid="checkTransfer"]')
      .click()

    cy.get('[data-test="open-auth-modal"]')
      .should("be.visible")
      .click()

    cy.get('[data-testid="result"]')
      .should("be.visible")
      .should("have.html", "Creditado com sucesso")

  });

})